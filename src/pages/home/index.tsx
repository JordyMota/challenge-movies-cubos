import React, { Component } from 'react';

import Searchbar from '../../components/searchbar';
import MovieList from '../../components/movieList';
import { Container } from './styles';

export default class Home extends Component {
  render() {
    return (
      <Container>
        <Searchbar />
        <MovieList />
      </Container>
    );
  }
}
