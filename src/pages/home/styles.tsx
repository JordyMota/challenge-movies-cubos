import styled from 'styled-components';
import * as global from '../../assets/styles';

export const Container = styled(global.Container)`
    width: 100%;
    align-items: center;
    padding: 0 6vw;
    @media ${ global.size.extraSmall } {
        padding: 0 5vw;
    }
`;