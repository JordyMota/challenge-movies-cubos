import { call, put } from 'redux-saga/effects';

import api, { nameRoute, genreRoute, defaultParams, getByGenreRoute } from '../../../services/api';
import { loadSucces, loadFailure } from './actions';
import { genre, Movie } from './types';

export function* load({payload}:any) {
    try {
        console.log(payload);
        const genreResponse = yield call(api.get,genreRoute+defaultParams);
        var response = yield call(api.get,nameRoute+defaultParams+'&query='+payload);
        const genreList:number[] = [];
        genreResponse.data.genres.forEach((item: genre)=> {
            let text = payload.split('+')[0];
            let cleanText = text.search('&page=');
            if(cleanText >= 0)
            text = text.slice(0,cleanText);
            if(item.name.toLowerCase().search(text) >= 0)
            genreList.push(item.id);
        });
        var byGenreResponse: any = [];
        if(genreList.length) {
            byGenreResponse = yield call(api.get,getByGenreRoute+(genreList.join('|')));
            byGenreResponse = byGenreResponse.data.results;
        }
        response = response.data.results.concat(byGenreResponse.filter(function (item: any) {
            return response.data.results.indexOf(item) < 0;
        }));
        response.forEach((movie: Movie)=> {
            let genres: genre[] = [];
            movie.genre_ids.forEach((item:number)=> {
                let genreItem = genreResponse.data.genres.find((genreItem: genre) => genreItem.id === item);
                genres.push(genreItem);
            });
            movie.genres = genres;
        });
        yield put(loadSucces(response));
    } catch (err) {
        console.log(err);
        yield put(loadFailure())
    }
}