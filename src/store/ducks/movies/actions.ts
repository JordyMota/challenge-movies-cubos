import { action } from 'typesafe-actions';
import { MoviesTypes, Movie } from './types';

export const loadRequest = (text: string) => action(MoviesTypes.LOAD_REQUEST, text);

export const loadSucces = (data: Movie[]) => action(MoviesTypes.LOAD_SUCCES, {data});

export const loadFailure = () => action(MoviesTypes.LOAD_FAILURE);
