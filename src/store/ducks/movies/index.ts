import { Reducer } from 'redux';
import { MoviesState, MoviesTypes } from './types';

const INITIAL_STATE: MoviesState = {
    data: [],
    loading: false,
    error: false,
    lastReasearch: '',
}

const reducer: Reducer<MoviesState> = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case MoviesTypes.LOAD_REQUEST:
            return {
                ...state,
                loading: true,
                lastReasearch: action.payload
            };

        case MoviesTypes.LOAD_SUCCES:
            return {
                ...state,
                loading: false,
                error: false,
                data: action.payload.data,
            };

        case MoviesTypes.LOAD_FAILURE:
            return {
                ...state,
                loading: false,
                error: true,
                data: [],
            };
        default:
            return state;
    }
}

export default reducer;
