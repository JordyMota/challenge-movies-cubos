import { all, takeLatest } from 'redux-saga/effects';

import { MoviesTypes } from './movies/types';
import * as movies from './movies/sagas';
import { SingleMoviesTypes } from './singleMovie/types';
import * as single from './singleMovie/sagas';

export default function* rootSaga() {
    return yield all([
        takeLatest(MoviesTypes.LOAD_REQUEST, (action)=>movies.load(action)),
        takeLatest(SingleMoviesTypes.LOAD_REQUEST, (action)=>single.load(action)),
    ]);
}