import { action } from 'typesafe-actions';
import { SingleMoviesTypes, SingleMovie } from './types';

export const loadRequest = (id: string) => action(SingleMoviesTypes.LOAD_REQUEST, id);

export const loadSucces = (data: SingleMovie) => action(SingleMoviesTypes.LOAD_SUCCES, {data});

export const loadFailure = () => action(SingleMoviesTypes.LOAD_FAILURE);