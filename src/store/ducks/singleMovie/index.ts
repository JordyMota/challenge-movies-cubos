import { Reducer } from 'redux';
import { SingleMoviesState, SingleMoviesTypes } from './types';

const INITIAL_STATE: SingleMoviesState = {
    data: null,
    loading: false,
    error: false,
}

const reducer: Reducer<SingleMoviesState> = (state = INITIAL_STATE, action) => {;
    switch (action.type) {
        case SingleMoviesTypes.LOAD_REQUEST:
            return {
                ...state,
                loading: true
            };

        case SingleMoviesTypes.LOAD_SUCCES:
            return {
                ...state,
                loading: false,
                error: false,
                data: action.payload.data
            };

        case SingleMoviesTypes.LOAD_FAILURE:
            return {
                ...state,
                loading: false,
                error: true,
                data: null,
            };
        default:
            return state;
    }
}

export default reducer;