import { combineReducers } from 'redux';

import movies from './movies';
import singleMovie from './singleMovie';

export default combineReducers({
    movies,
    singleMovie,
});