import { createStore, applyMiddleware, Store } from 'redux';
import createSagaMiddleware from 'redux-saga';

import { MoviesState }  from './ducks/movies/types';
import { SingleMoviesState }  from './ducks/singleMovie/types';
import rootReducer from './ducks/rootReducers';
import rootSaga from './ducks/rootSaga';

export interface AplicationState {
    movies: MoviesState
    singleMovie: SingleMoviesState
}

const sagaMiddleware = createSagaMiddleware()
const store :Store<AplicationState> = createStore(rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

export default store;

