import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import { AplicationState } from '../../store';
import { Movie } from '../../store/ducks/movies/types';
import * as MovieActions from '../../store/ducks/movies/actions';
import MovieItem from '../movieItem';
import Pagination from '../pagination';

import { MovieListContainer } from './styles';

interface DispatchProps {
  loadRequest(text: string): void
  loadSucces(movies: Props["movies"]): void
}

interface StateProps {
    movies: Movie[]
    lastReasearch: string,
    loading: boolean
  }
  
  interface OwnProps {
    page: number
    searchPage: number
}

type Props = StateProps & DispatchProps;

var paginationsMovies :Movie[] = [];
var reset: boolean = false;

class MovieList extends Component<Props> {

  state: OwnProps = {
    page: 1,
    searchPage: 1,
  }
  
  componentDidMount() {
    paginationsMovies = this.getPageMovies(this.props.movies,this.state.page);

    this.handlePageChange = this.handlePageChange.bind(this);
    this.getPageMovies = this.getPageMovies.bind(this);
  }

  handlePageChange = async (
    forward = true,
    searchString: string,
    loadRequest: Props["loadRequest"]
  )=> {
    if(reset)
      reset = !reset;
    let searchParam = '&page=';
    let willSearch = false;
    if(forward) {
      await this.setState({ page: this.state.page+1 });
      if((this.state.page-1)%4 === 0) {
        await this.setState({
          searchPage: this.state.searchPage+1,
          page: 1,
        });
        willSearch = true;
      }
    } else {
      if(this.state.page === 1 && this.state.searchPage === 1)
       return;
      await this.setState({ page: this.state.page-1 });
      if(this.state.page === 0) {
        await this.setState({
          searchPage: this.state.searchPage-1,
          page: 4,
        });
        willSearch = true;
      }
    }
    window.scrollTo(0,0);
    if(willSearch) {
      let cleanText = searchString.search('&page=');
      if(cleanText >= 0)
        searchString = searchString.slice(0,cleanText);
      // searchString = searchString.replace((searchParam+(this.state.searchPage-1)),'');
      searchString += searchParam+this.state.searchPage;
      loadRequest(searchString);
    }
  }

  getPageMovies = (movies: StateProps["movies"],page: number)=> {
    return movies.slice((5*(page-1)),(5*page));
  }

  renderMessages = ({ movies, lastReasearch, loadRequest, loading }: Props)=> {
    const handlePaginate = async (direction=true,amount=1)=> {
      for(let i = 0; i < amount; i++) {
        await this.handlePageChange(direction,lastReasearch,loadRequest);
      }
    }
    let response;
    if(!loading) {
      if(!movies.length) {
        if (this.state.searchPage !== 1) {
          setTimeout(()=> {
            this.handlePageChange(false,lastReasearch,loadRequest);
          },400);
          paginationsMovies = [];
          return 'Redirecionando...';
        }
        return ('Nenhum filme encontrado');
      }
      paginationsMovies = this.getPageMovies(this.props.movies,this.state.page);
      response = (
      <MovieListContainer>
        { 
          this.getPageMovies(movies,this.state.page).map(movie => {
            const result = (<MovieItem key={movie.id} movie={movie}/>)
            return result;
          })
          
        }
        <Pagination
          movies={paginationsMovies}
          handlePaginate={handlePaginate}
          reset={reset}
        />
      </MovieListContainer>);
      reset = false;
      return response;
    }
    return ('Carregando...');
  }

  render() {
    return this.renderMessages(this.props);
  }
}

const mapStateToProps = ({ movies }: AplicationState) => {
  paginationsMovies = [];
  reset = true;
  if(movies.lastReasearch.search('&page=') > -1) {
    reset = false;
  }
  return ({
    movies: movies.data,
    lastReasearch: movies.lastReasearch,
    loading: movies.loading,
  });
};

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(MovieActions, dispatch);

export default connect(mapStateToProps,mapDispatchToProps)(MovieList);
