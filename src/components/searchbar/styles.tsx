import styled from 'styled-components';
import * as global from '../../assets/styles';

export const SearchbarContainer = styled.form`
    display: flex;
    width: 100%;
    min-width: 100%;
    background-color: ${ global.colors.support };
    border-radius: 30px;
    margin-top: 7vh;
    overflow: hidden;
    input {
        flex: 1;
        background-color: transparent;
        outline: none !important;
        border: none !important;
        color: ${ global.colors.supportType };
        padding: 15px 25px;
        overflow: hidden;
        font-size: 1rem;
        ::placeholder {
            color: ${ global.colors.primary };
            opacity: .5;
        }
        @media ${ global.size.extraSmall } {
            font-size: .89rem;
        }
    }
`;