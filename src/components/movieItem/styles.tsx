import styled from 'styled-components';
import * as global from '../../assets/styles';

export const MovieContainer = styled.li`
    position: relative;
    width: 100%;
    height: auto;
    list-style: none;
    display: flex;
    margin-bottom: 7vh;
    border-radius: 2px;
    overflow: hidden;
    a {
        text-decoration: none !important;
        height: auto;
        width: 100%;
        background-color: ${global.colors.support};
        display: flex;
    }
    `;

export const MoviePoster = styled.img`
    width: 20.02vw;
    max-width: 20.02vw;
    height: auto;
    max-height: 30.078vw;
    @media ${ global.size.extraSmall } {
        width: 38.02vw;
        max-width: 38.02vw;
        max-height: 57.121vw;
    }
`;

export const MovieContent = styled(global.Container)`
    position: relative;
    flex: 1;
`;

export const MovieTitle = styled(global.RowContainer)`
    width: 100%;
    max-height: 65px;
    min-height: 65px;
    align-items: flex-end;
    padding-bottom: 6px;
    padding-left: 9.57vw;
    position: relative;
    background-color: ${global.colors.primary};
    h2 {
        text-transform: capitalize;
        font-weight: normal;
        color: ${global.colors.secondary};
        font-size: 1.85rem;
        margin: 0;
        transform: scaleY(1.1);
    }
    @media ${ global.size.extraSmall } {
        max-height: 40px;
        min-height: 40px;
        padding-left: 30px;
        h2 {
            font-size: 1rem;
        }
    }
`;

export const MovieRate = styled(global.CenterContainer)<global.Props>`
    width: 6.641vw;
    height: 6.641vw;
    background-color: ${global.colors.primary};
    position: absolute;
    border-radius: 50%;
    overflow: hidden;
    left: 1.465vw;
    margin-top: 1.3vw;
    display: ${(props: global.Props) => props.toggleVisibleXS ? 'none' : 'flex'};
    z-index: 18;
    div {
        width: 6.348vw;
        height: 6.348vw;
        border-radius: 50%;
        border: 5px solid ${global.colors.secondary};
        display: flex;
        justify-content: center;
        align-items: center;
        span {
            font-size: 1.5rem;
            color: ${global.colors.secondary};
        }
    }
    @media ${ global.size.large } {
        display: ${(props: global.Props) => props.toggleVisibleXS ? 'none' : 'flex'};
        margin-top: 1.95vw;
        div {
            border-width: 4px;
            span {
                font-size: 1.4rem;
            }
        }
    }
    @media ${ global.size.medium } {
        display: ${(props: global.Props) => props.toggleVisibleXS ? 'none' : 'flex'};
        margin-top: 3.2vw;
        div {
            border-width: 3px;
            span {
                font-size: 1.2rem;
            }
        }    
    }
    @media ${ global.size.small } {
        display: ${(props: global.Props) => props.toggleVisibleXS ? 'none' : 'flex'};
        margin-top: 5.6vw;
        div {
            border-width: 3px;
            span {
                font-size: 1rem;
            }
        }   
    }
    @media ${ global.size.extraSmall } {
        display: ${(props: global.Props) => props.toggleVisibleXS ? 'flex' : 'none'};
        margin-top: 3.9vw;
        left: 30.665vw;
        width: 15.625vw;
        height: 15.625vw;
        div {
            width: 13.125vw;
            height: 13.125vw;
            border-width: 2px;
            span {
                font-size: .9rem;
            }
        }
    }
`;

export const ReleaseDate = styled.p`
    margin: 0;
    margin-left: calc(9.57vw - 3px);
    font-size: 1.16rem;
    margin-top: 8px;
    opacity: .7;
    color: ${global.colors.supportType};
    @media ${ global.size.extraSmall } {
        margin-left: 9.375vw;
        margin-top: 3px;
        font-size: 0.76rem;
    }
`;

export const MovieDescript = styled.p`
    white-space: normal;
    margin: 7vh 20px 0;
    color: ${global.colors.supportType};
    font-size: 1rem;
    max-height: 21vh;
    overflow: hidden;
    position: relative;
    @media ${ global.size.extraSmall }, ${ global.size.small }, ${ global.size.medium } {
        max-height: 20vw;
        margin: 3vh 16px 0;
        font-size: .69rem;
        :before {
            content: '';
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 16px;
            background: linear-gradient(to top, rgba(235,235,235,.95), rgba(235,235,235,.65), rgba(235,235,235,.35));
        }
    }
`;

export const GenreList = styled(global.RowContainer)`
    margin: 3vh 20px 0;
    flex-wrap: nowrap;
    overflow-x: auto;
    max-width: calc(100% - 40px);
    span {
        padding: 6px 12px;
        background-color: #fff;
        color: ${global.colors.primary};
        border: 1px solid ${global.colors.primary};
        border-radius: 20px;
        margin-right: 2%;
        font-size: .75rem;
    }    
`;