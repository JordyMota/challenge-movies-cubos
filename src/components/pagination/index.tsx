import React from 'react';

import { Movie } from '../../store/ducks/movies/types';
import { PaginationList, PaginationItem } from './styles';

interface StateProps {
    movies: Movie[]
    handlePaginate(direction: boolean, amount?: number): void
    reset: boolean
}

var active: number = 1;

const calculatePaginations = ({movies}: StateProps)=> {
    const noNext = movies.length < 5;
    let paginationsOptions = [1,2,3,4,5];
    let paginations: number[];
    if((active - 2) < 3) {
        paginations = paginationsOptions.slice(0,active);
        if(!noNext)
            paginations = paginations.concat(
                paginationsOptions.slice(
                    active,
                    paginationsOptions.length
                )
            );
        return paginations;
    }
    paginations = [active-2,active-1,active];
    if(!noNext)
        paginations = paginations.concat([active+1,active+2]);
    return paginations;
}

const handleClick = ({handlePaginate, movies}:StateProps, item: number)=> {
    handlePaginate(
        (item > active),
        Math.abs((active - item))
    );
    active = item;
}

const Pagination = (props: StateProps)=> {
    var local: any = {};
    local.handlePaginate = props.handlePaginate;
    if(props.reset) {
        active = 1;
    }
    active = props.movies.length ? active : (active - 1);
    return (
        <PaginationList>
            {calculatePaginations(props).map(
                item => {
                    if(item === active)
                        return (
                            <PaginationItem key={item}>
                                <div>
                                    <span>
                                        {item}
                                    </span>
                                </div>
                            </PaginationItem>
                        );
                    return (
                        // eslint-disable-next-line
                        <a
                            className="pagination-bullet"
                            onClick={()=>handleClick(local,item)}
                            key={item}
                        >
                            {item}
                        </a>)
                })
            }
        </PaginationList>
    );
}


export default Pagination;