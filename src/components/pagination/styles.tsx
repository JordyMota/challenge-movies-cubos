import styled from 'styled-components';
import * as global from '../../assets/styles';

export const PaginationList = styled(global.CenterContainer)`
    width: 100%;
    position: absolute;
    bottom: 17px;
    left: 0;
    a.pagination-bullet {
        text-decoration: none;
        color: ${global.colors.primary};
        font-size: 1.3rem;
        margin: 0 8px;
        :hover {
            cursor: pointer;
            text-decoration: underline;
        }
    }
`;
export const PaginationItem = styled.a`
    width: 53px;
    display: flex;
    align-items: center;
    justify-content: center;
    height: 53px;
    background-color: ${global.colors.primary};
    border-radius: 50%;
    overflow: hidden;
    margin: 0 8px;
    :hover {
        cursor: pointer;
    }
    div {
        border-radius: 50%;
        display: flex;
        align-items: center;
        justify-content: center;
        width: 48px;
        height: 48px;
        border: 4px solid ${global.colors.secondary};
        span {
            font-size: 1.6rem;
            color: ${global.colors.secondary};
        }
    }
`;
