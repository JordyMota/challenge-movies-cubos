import axios from 'axios';

const api = axios.create({
    baseURL: 'https://api.themoviedb.org/3/'
});

export const genreRoute = 'genre/movie/list'; 
export const nameRoute = 'search/movie';
export const singleRoute = 'movie/';
export const defaultParams = '?api_key=29ef26bbeb05d990f7797d4ffdf2df0f&language=pt-BR';
export const imgRoute = 'https://image.tmdb.org/t/p/w500';
export const getByGenreRoute = 'discover/movie'+defaultParams+'&with_genres=';
export default api;