import React from 'react';
import { Provider } from 'react-redux';

import Routes from './routes';
import { AppContainer, AppHeader, AppContent } from './assets/styles';
import store from './store';

const App: React.FC = () => {
  return (
      <Provider
        store={store}
      >
        <AppContainer>
          <AppHeader>
            <h1
              className="font-titillium"
            >
              Movies
            </h1>
          </AppHeader>
          <AppContent>
            <Routes />
          </AppContent>
        </AppContainer>
      </Provider>
  );
}

export default App;
